/**
 * Server side code.
 */
"use strict";

var express = require('express');
var bodyParser = require("body-parser");
var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 3009;
app.use(express.static(__dirname + "/../client/"));

app.get("quiz_data.json", function(req, res) 
{res.json(quiz_data.json);
    // else???
});

app.use(function(req, res) {
    res.send("<h1>!!!! Page not found ! ! !</h1>");
});

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});

// allow this encapsulated code to be accessible/utlised by other files
module.exports = app
